import { RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { LoginComponent } from './pages/login/login.component';

const routes = [
    { path: '', component: LoginComponent }
];

export const Routes: ModuleWithProviders = RouterModule.forRoot(routes);